<?php
/*
 * Plugin Name: WooCommerce download with redirect and nonce validation
 * Requires Plugins: woocommerce
 * Version: 2.0.1
 * Description: Make downloads redirect <em>and</em> secure with nonce when you append <code>wc_redirect_nonce=_nonce_</code> to the url querystring. The <code>_nonce_</code> part of the string is replaced by a nonce string. You can validate the nonce using <code>verify_wc_redirect_nonce()</code> (please check if function exists). <strong>In multisite, activate this plugin on all blogs you are linking to.</strong>
 * Author: Joost de Keijzer
 * Author URI: https://dkzr.nl
 * Plugin URI: https://dkzr.nl
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/woo-download-with-nonce
 * Text Domain: woo-download-with-nonce
 * Domain Path: /languages
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

if ( ! defined( 'ABSPATH' ) ) { exit; /* Exit if accessed directly */ }

class dkzrWooDownloadRedirectNonce {

  /**
   * Cookie name used for the session.
   *
   * @var string cookie name
   */
  protected $_cookie;

  /**
   * Expiration in seconds for transient and cookie
   *
   * @var int seconds to expire
   */
  protected $_expire;

  public function __construct() {
    add_action( 'init', [ $this, 'init' ] );

    add_filter( 'query_vars', [ $this, 'query_vars' ], 10, 1 );
    add_filter( 'woocommerce_download_product_filepath', [ $this, 'woocommerce_download_product_filepath' ], 10, 5 );
    add_filter( 'woocommerce_file_download_method', [ $this, 'woocommerce_file_download_method' ], 10, 3 );
  }

  public function verify_nonce() {
    $order_key = get_query_var( 'wc_order' );
    $nonce = get_query_var( 'wc_redirect_nonce' );
    if ( $order_key && $nonce ) {
      $transient = sprintf( 'wc_redirect_nonce_for_%s', $order_key );
      $expected = get_site_transient( $transient );

      return isset( $expected['key'], $expected['value'] ) && $this->validate_cookie( $expected['key'] ) && hash_equals( $expected['value'], $nonce );
    }

    return false;
  }

  /**
   * Copied from WC_Session_Handler::get_session_cookie
   */
  protected function validate_cookie( $nonce_key ) {
    $cookie_value = isset( $_COOKIE[ $this->_cookie ] ) ? wp_unslash( $_COOKIE[ $this->_cookie ] ) : false; // @codingStandardsIgnoreLine.
    if ( empty( $cookie_value ) || ! is_string( $cookie_value ) ) {
      return false;
    }
    list( $session_expiration, $cookie_hash ) = explode( '||', $cookie_value );

    if ( empty( $session_expiration ) || $session_expiration < time() ) {
      return false;
    }

    // Validate hash.
    $to_hash = $nonce_key . '|' . $session_expiration;
    $hash    = hash_hmac( 'md5', $to_hash, wp_hash( $to_hash ) );

    if ( empty( $cookie_hash ) || ! hash_equals( $hash, $cookie_hash ) ) {
      return false;
    }

    return true;
  }

  // MARK: Actions

  public function init() {
    $this->_cookie = 'wc_redirect_download_' . COOKIEHASH;
    $this->_expire = 6 * HOUR_IN_SECONDS;
  }

  // MARK: Filters

  public function query_vars( $query_vars ) {
    $query_vars[] = 'wc_redirect_nonce';
    $query_vars[] = 'wc_order';

    return $query_vars;
  }

  public function woocommerce_download_product_filepath( $file_path, $email_address, $order, $product, $download ) {
    $order_key = $order->get_order_key();
    if ( $order_key && false !== strpos( $file_path, 'wc_redirect_nonce=_nonce_' ) ) {
      require_once ABSPATH . 'wp-includes/class-phpass.php';
      $hasher    = new PasswordHash( 8, false );
      $nonce     = md5( $hasher->get_random_bytes( 32 ) );
      $nonce_key = md5( $hasher->get_random_bytes( 32 ) );

      $transient = sprintf( 'wc_redirect_nonce_for_%s', $order_key );
      set_site_transient( $transient, [ 'key' => $nonce_key, 'value' => $nonce ], $this->_expire );

      $cookie_expiration = time() + $this->_expire;
      $to_hash           = $nonce_key . '|' . $cookie_expiration;
      $cookie_hash       = hash_hmac( 'md5', $to_hash, wp_hash( $to_hash ) );
      $cookie_value      = $cookie_expiration . '||' . $cookie_hash;
      wc_setcookie( $this->_cookie, $cookie_value, $cookie_expiration, false, true );

      $file_path = str_replace( 'wc_redirect_nonce=_nonce_', sprintf( 'wc_redirect_nonce=%s&wc_order=%s', $nonce, $order_key ), $file_path );
    }

    return $file_path;
  }

  public function woocommerce_file_download_method( $file_download_method, $product_id, $file_path ) {
    if ( false !== strpos( $file_path, 'wc_redirect_nonce=' ) && false !== strpos( $file_path, 'wc_order=' ) ) {
      $file_download_method = 'redirect';
    }

    return $file_download_method;
  }
}

$dkzrWooDownloadRedirectNonce = new dkzrWooDownloadRedirectNonce();

/**
 * Verify wc_redirect_nonce, if it exists.
 */
function verify_wc_redirect_nonce() {
  return $GLOBALS['dkzrWooDownloadRedirectNonce']->verify_nonce();
}
